﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompteurBiblio;

namespace CompteutTests
{
    [TestClass]
    public class Compteurtest
    {
        [TestMethod]
        public void TestIncre()
        {
            Compteurclass.IncrementCompteur();
            Assert.AreEqual(1, Compteurclass.compteur);

        }

        [TestMethod]
        public void TestDecre()
        {
            Compteurclass.DecrementCompteur();
            Assert.AreEqual(0, Compteurclass.compteur);
            Compteurclass.DecrementCompteur();
            Assert.AreEqual(0, Compteurclass.compteur);
        }

        [TestMethod]
        public void TestReboot()
        {
            Compteurclass.IncrementCompteur();
            Compteurclass.RebootCompteur();
            Assert.AreEqual(0, Compteurclass.compteur);
        }
    }
}
