﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompteurBiblio
{
    public class Compteurclass
    {
        public static int compteur=0;


        public static void IncrementCompteur()
        {
            compteur = compteur + 1;
        }

        public static void DecrementCompteur()
        {
            if (compteur > 0)
            {
                compteur = compteur - 1;
            }
        }

        public static void RebootCompteur()
        {
            compteur = 0;
        }
    }
}
