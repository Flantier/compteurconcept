﻿Imports CompteurBiblio

Public Class Form1

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Compteurclass.DecrementCompteur()
        Label2.Text = Compteurclass.compteur
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Compteurclass.IncrementCompteur()
        Label2.Text = Compteurclass.compteur
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Compteurclass.RebootCompteur()
        Label2.Text = Compteurclass.compteur
    End Sub

End Class
